package com.github.soshibby.volvo;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
 
@Configuration
@ComponentScan("com.github.soshibby.volvo")
public class Config {
 
}