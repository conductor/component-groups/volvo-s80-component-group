package com.github.soshibby.volvo.canbus.handler.ecu.dim.components;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.soshibby.volvo.canbus.sender.CanSender;
import com.github.soshibby.volvo.canbus.types.DimMessage;

public class Dim {
	private Logger log = LogManager.getLogger(Dim.class.getName());
	private String text = null;
	private CanSender canbusSender;

	public void handleMessage(DimMessage message) {
		if (text != null && message.getCounter() == 0) {
			sendDimText(text);
		}
	}

	public void displayText(String text) {
		this.text = text;
	}
	
	private void sendDimText(String strMessage) {
		byte[] message = strMessage.getBytes();
		
		try {
			canbusSender.send(0x00B00006, new byte[] { 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x25 });	//Turn on the dim
		
			byte[] data = new byte[] { (byte) 0x97, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			appendToList(data, 2, message, 0, 6);
			canbusSender.send(0x00300008, data);
			
			data = new byte[] { 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			appendToList(data, 1, message, 6, 7);
			canbusSender.send(0x00300008, data);
			
			data = new byte[] { 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			appendToList(data, 1, message, 13, 7);
			canbusSender.send(0x00300008, data);
			
			data = new byte[] { 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			appendToList(data, 1, message, 20, 7);
			canbusSender.send(0x00300008, data);
			
			data = new byte[] { 0x56, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			appendToList(data, 1, message, 27, 7);
			canbusSender.send(0x00300008, data);
		} catch (Exception e) {
			log.error("Failed to send data to canbus.", e);
		}
	}
	
	private byte[] appendToList(byte[] appendTo, int startIndex, byte[] data, int start, int count){
		int i = startIndex;
		for(int x = start; x < start + count; x++){
			if(x < data.length)
				appendTo[i] = data[x];
			else
				break;
			i++;
		}
		
		return appendTo;
	}

}
