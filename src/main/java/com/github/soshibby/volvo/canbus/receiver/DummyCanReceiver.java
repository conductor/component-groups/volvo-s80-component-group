package com.github.soshibby.volvo.canbus.receiver;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.github.soshibby.volvo.canbus.handler.CanMessageHandler;
import com.github.soshibby.volvo.canbus.parser.CanMessageParser;
import com.github.soshibby.volvo.canbus.types.AlarmMessage;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.enums.ECU;

@Service("dummyCanReceiver")
public class DummyCanReceiver implements ICanReceiver {
	private static Logger log = LogManager.getLogger(DummyCanReceiver.class.getName());
	private CanMessageParser canMessageParser = new CanMessageParser();
	
	@Inject
	private CanMessageHandler canMessageHandler = new CanMessageHandler();

	@Override
	public void listen() {
		log.info("Dummy can receiver is listening");
		
		List<CanMessage> canMessages = new ArrayList<CanMessage>();
		AlarmMessage alarmMessage = new AlarmMessage(ECU.CENTRAL);
		alarmMessage.setAlarmTriggered(true);
		canMessages.add(alarmMessage); 
		
		canMessageHandler.handle(canMessages);
	}

}
