package com.github.soshibby.volvo.canbus.handler.ecu.central;

import javax.inject.Inject;
import javax.inject.Named;

import com.github.soshibby.volvo.canbus.handler.ecu.central.components.Alarm;
import com.github.soshibby.volvo.canbus.handler.ecu.central.components.Doors;
import com.github.soshibby.volvo.canbus.handler.ecu.central.components.Speedometer;
import com.github.soshibby.volvo.canbus.sender.CanSender;
import com.github.soshibby.volvo.canbus.types.AlarmMessage;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.DoorMessage;
import com.github.soshibby.volvo.canbus.types.SpeedometerMessage;

@Named
public class CentralECU {
	Alarm alarm;
	Doors doors;
	Speedometer speedometer;

	@Inject
	public CentralECU(CanSender canSender) {
	  this.alarm = new Alarm();
	  this.doors = new Doors(canSender);
	  this.speedometer = new Speedometer();
	}

	public void handleMessage(CanMessage message) {
		if (message instanceof AlarmMessage) {
			alarm.handleMessage((AlarmMessage) message);
		} else if (message instanceof DoorMessage) {
			doors.handleMessage((DoorMessage) message);
		} else if (message instanceof SpeedometerMessage) {
			speedometer.handleMessage((SpeedometerMessage) message);
		}
	}

	public Alarm getAlarm() {
		return alarm;
	}

	public Doors getDoors() {
		return doors;
	}

	public Speedometer getSpeedometer() {
		return speedometer;
	}

}
