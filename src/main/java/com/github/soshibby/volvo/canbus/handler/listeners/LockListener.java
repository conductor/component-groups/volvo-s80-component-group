package com.github.soshibby.volvo.canbus.handler.listeners;

public interface LockListener {
	public void onLockStatusChanged(boolean isLocked);
}
