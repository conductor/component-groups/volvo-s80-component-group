package com.github.soshibby.volvo.canbus.parser.ecu;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.parser.ICanMessageParser;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.EngineMessage;
import com.github.soshibby.volvo.canbus.types.enums.ECU;

import de.entropia.can.CanSocket.CanFrame;

public class EngineECUParser implements ICanMessageParser {

	@Override
	public List<CanMessage> parse(CanFrame frame) {
		byte[] data = frame.getData();
		List<CanMessage> messages = new ArrayList<CanMessage>();

		messages.add(parseRPMMessage(data));

		return messages;
	}
		
	public EngineMessage parseRPMMessage(byte[] data) {
		EngineMessage message = new EngineMessage(ECU.ENGINE);
		
		// Convert RPM to decimal (the rpm value of 0d is 2000h)
		int rpm = 0;
		rpm += data[6] * 0x100;
		rpm += data[7];
		rpm -= 0x2000;
		
		message.setRpm(rpm);
		return message;
	}
}
