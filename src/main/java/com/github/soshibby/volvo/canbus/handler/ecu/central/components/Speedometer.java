package com.github.soshibby.volvo.canbus.handler.ecu.central.components;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.handler.listeners.SpeedometerListener;
import com.github.soshibby.volvo.canbus.types.SpeedometerMessage;

public class Speedometer {
	private List<SpeedometerListener> listeners = new ArrayList<SpeedometerListener>();
	private SpeedometerMessage previousMessage;

	public void handleMessage(SpeedometerMessage message) {
		if (hasCurrentSpeedChanged(message)) {
			for (SpeedometerListener listener : this.listeners) {
				listener.onCurrentSpeedChanged(message.getCurrentSpeed());
			}
		}

		previousMessage = message;
	}

	private boolean hasCurrentSpeedChanged(SpeedometerMessage speedometerMessage) {
		if (previousMessage == null) {
			return true;
		}

		return speedometerMessage.getCurrentSpeed() != previousMessage.getCurrentSpeed();
	}

	public void addListener(SpeedometerListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(SpeedometerListener listener) {
		this.listeners.remove(listener);
	}
}
