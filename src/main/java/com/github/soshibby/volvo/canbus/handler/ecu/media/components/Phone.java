package com.github.soshibby.volvo.canbus.handler.ecu.media.components;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.handler.listeners.PhoneListener;
import com.github.soshibby.volvo.canbus.types.PhoneMessage;

public class Phone {
	private List<PhoneListener> listeners = new ArrayList<PhoneListener>();
	private PhoneMessage previousMessage;

	public void handleMessage(PhoneMessage message) {
		if (hasAnswerCallChanged(message)) {
			for (PhoneListener listener : this.listeners) {
				listener.onAnswerCallChanged(message.isAnswerCall());
			}
		}
		
		if (hasRejectCallChanged(message)) {
			for (PhoneListener listener : this.listeners) {
				listener.onRejectCallChanged(message.isRejectCall());
			}
		}
		
		previousMessage = message;
	}

	private boolean hasAnswerCallChanged(PhoneMessage phoneMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isAnswerCall() != phoneMessage.isAnswerCall();
	}
	
	private boolean hasRejectCallChanged(PhoneMessage phoneMessage) {
		if(previousMessage == null) {
			return true;
		}
		
		return previousMessage.isRejectCall() != phoneMessage.isRejectCall();
	}

	public void addListener(PhoneListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(PhoneListener listener) {
		this.listeners.remove(listener);
	}
}
