package com.github.soshibby.volvo.canbus.handler.listeners;

public interface RadioListener {
	public void onNextTrackPressed(boolean nextTrackPressed);
	public void onPreviousTrackPressed(boolean previousTrackPressed);
	public void onIncreaseVolumePressed(boolean increaseVolumePressed);
	public void onDecreaseVolumePressed(boolean decreaseVolumePressed);
}
