package com.github.soshibby.volvo.canbus.utils;

public final class ByteUtil {

	public static byte getLowNibble(byte data) {
		return (byte) (data & 0x0f);
	}

	public static byte getHighNibble(byte data) {
		return (byte) ((byte) (data >> 4) & 0x0f);
	}

	public static boolean isBitSet(byte b, int pos) {
		return (b & (1 << pos)) != 0;
	}
}
