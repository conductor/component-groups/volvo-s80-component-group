package com.github.soshibby.volvo.canbus.parser.ecu;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.parser.ICanMessageParser;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.DimMessage;
import com.github.soshibby.volvo.canbus.types.enums.ECU;
import com.github.soshibby.volvo.canbus.utils.ByteUtil;

import de.entropia.can.CanSocket.CanFrame;

public class DimECUParser implements ICanMessageParser {
	
	public List<CanMessage> parse(CanFrame frame) {
		byte[] data = frame.getData();
		List<CanMessage> messages = new ArrayList<CanMessage>();

		messages.add(parseDimMessage(data));

		return messages;
	}
	
	public DimMessage parseDimMessage(byte[] data) {
		DimMessage dim = new DimMessage(ECU.DIM);
		byte counter = ByteUtil.getHighNibble(data[0]);
		dim.setCounter(counter);
        return dim;
	}

}
