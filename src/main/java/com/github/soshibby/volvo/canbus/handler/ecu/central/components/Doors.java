package com.github.soshibby.volvo.canbus.handler.ecu.central.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.soshibby.volvo.canbus.handler.listeners.DoorOpenListener;
import com.github.soshibby.volvo.canbus.sender.CanSender;
import com.github.soshibby.volvo.canbus.types.DoorMessage;

public class Doors {
  private Logger log = LogManager.getLogger(Doors.class.getName());
  private CanSender canbusSender;
  private List<DoorOpenListener> listeners = new ArrayList<DoorOpenListener>();
  private DoorMessage previousMessage;
  private boolean doUnlockDoors;
  private boolean doLockDoors;

  public Doors(CanSender canSender) {
    this.canbusSender = canSender;
    log.info("Canbus sender: " + canSender);
  }

  public void handleMessage(DoorMessage message) {
    if (doLockDoors && message.getCounter() == 0x0) {
      log.info("Waking up canbus and locking doors.");
      sendLockDoors();
      doLockDoors = false;
    }

    if (doUnlockDoors && message.getCounter() == 0x0) {
      log.info("Waking up canbus and unlocking doors.");
      sendUnlockDoors();
      doUnlockDoors = false;
    }

    if (hasDriverDoorChanged(message)) {
      for (DoorOpenListener listener : this.listeners) {
        listener.onDriverDoorOpenChanged(message.isDriverDoorOpen());
      }
    }

    if (hasPassengerDoorChanged(message)) {
      for (DoorOpenListener listener : this.listeners) {
        listener.onPassengerDoorOpenChanged(message.isPassengerDoorOpen());
      }
    }

    if (hasRearLeftDoorChanged(message)) {
      for (DoorOpenListener listener : this.listeners) {
        listener.onRearLeftDoorOpenChanged(message.isRearLeftDoorOpen());
      }
    }

    if (hasRearRightDoorChanged(message)) {
      for (DoorOpenListener listener : this.listeners) {
        listener.onRearRightDoorOpenChanged(message.isRearRightDoorOpen());
      }
    }

    previousMessage = message;
  }

  public void lockDoors() {
    log.info("doLockDoors");
    wakeUpCanBus();
    doLockDoors = true;
  }

  public void unlockDoors() {
    log.info("doUnlockDoors");
    wakeUpCanBus();
    doUnlockDoors = true;
  }

  private void sendLockDoors() {
    try {
      canbusSender.send(0x00E01202, new byte[] { 0x40, 0x50, 0x00, (byte) 0xB0, 0x06, 0x00, 0x00, 0x00 });
    } catch (Exception e) {
      log.error("Failed to send data to canbus.", e);
    }
  }

  private void sendUnlockDoors() {
    try {
      canbusSender.send(0x00E01202, new byte[] { 0x40, 0x50, 0x00, (byte) 0xA8, 0x37, 0x00, 0x00, 0x00 });
    } catch (Exception e) {
      log.error("Failed to send data to canbus.", e);
    }
  }

  private void wakeUpCanBus() {
    try {
      canbusSender.send(0x00E01202, new byte[] { (byte) 0x80, 0x30, 0x00, 0x20, 0x07, 0x00, 0x00, 0x00 });
      canbusSender.send(0x00E01202, new byte[] { (byte) 0xC0, 0x70, 0x00, 0x20, 0x07, 0x00, 0x00, 0x00 });
    } catch (Exception e) {
      log.error("Failed to send data to canbus.", e);
    }
  }

  private boolean hasDriverDoorChanged(DoorMessage doorMessage) {
    if (previousMessage == null) {
      return true;
    }

    return previousMessage.isDriverDoorOpen() != doorMessage.isDriverDoorOpen();
  }

  private boolean hasPassengerDoorChanged(DoorMessage doorMessage) {
    if (previousMessage == null) {
      return true;
    }

    return previousMessage.isPassengerDoorOpen() != doorMessage.isPassengerDoorOpen();
  }

  private boolean hasRearLeftDoorChanged(DoorMessage doorMessage) {
    if (previousMessage == null) {
      return true;
    }

    return previousMessage.isRearLeftDoorOpen() != doorMessage.isRearLeftDoorOpen();
  }

  private boolean hasRearRightDoorChanged(DoorMessage doorMessage) {
    if (previousMessage == null) {
      return true;
    }

    return previousMessage.isRearRightDoorOpen() != doorMessage.isRearRightDoorOpen();
  }

  public void addListener(DoorOpenListener listener) {
    this.listeners.add(listener);
  }

  public void removeListener(DoorOpenListener listener) {
    this.listeners.remove(listener);
  }
}
