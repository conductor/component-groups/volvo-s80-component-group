package com.github.soshibby.volvo.canbus.parser;

import java.util.List;

import com.github.soshibby.volvo.canbus.types.CanMessage;

import de.entropia.can.CanSocket.CanFrame;

public interface ICanMessageParser {
	public List<CanMessage> parse(CanFrame frame);
}
