package com.github.soshibby.volvo.canbus.receiver;

import java.util.List;

import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.github.soshibby.volvo.canbus.handler.CanMessageHandler;
import com.github.soshibby.volvo.canbus.parser.CanMessageParser;
import com.github.soshibby.volvo.canbus.types.CanMessage;

import de.entropia.can.CanSocket;
import de.entropia.can.CanSocket.CanFrame;
import de.entropia.can.CanSocket.CanInterface;
import de.entropia.can.CanSocket.Mode;

@Service("canReceiver")
public class CanReceiver implements ICanReceiver {

  private static Logger log = LogManager.getLogger(CanReceiver.class.getName());
  private String canInterface = "can0";
	private Thread receiveCanMessagesThread = null;
	private CanMessageParser canMessageParser = new CanMessageParser();

	@Inject
	private CanMessageHandler canMessageHandler = new CanMessageHandler();

	public void listen() {
		listen(canInterface);
	}

	public void listen(String canInterface) {
		receiveCanMessagesThread = new Thread(new ReceiveCanMessages());
		receiveCanMessagesThread.setDaemon(true);
		receiveCanMessagesThread.start();
	}

	protected class ReceiveCanMessages implements Runnable {

		public void run() {
			log.info("Start listening for can messages.");

			try (final CanSocket socket = new CanSocket(Mode.RAW)) {
				final CanInterface canif = new CanInterface(socket, canInterface);
				socket.bind(canif);

				log.info("Successfully binded to can interface.");

				while (!Thread.currentThread().isInterrupted()) {
					CanFrame frame = socket.recv();
					List<CanMessage> messages = canMessageParser.parse(frame);
					canMessageHandler.handle(messages);
				}
			} catch (Exception e) {
				log.error("An error occured while reading can messages.", e);
			}

			log.info("Stopped listening for can messages!");
		}
	}
}
