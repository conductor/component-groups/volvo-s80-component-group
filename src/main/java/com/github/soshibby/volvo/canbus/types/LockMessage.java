package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.Door;
import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class LockMessage extends CanMessage {
	private Door door;
	private boolean locked;
	
	public LockMessage(ECU ecu) {
		super(ecu);
	}

	public Door getDoor() {
		return door;
	}

	public void setDoor(Door door) {
		this.door = door;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

}
