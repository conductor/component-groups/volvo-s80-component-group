package com.github.soshibby.volvo.canbus.handler.ecu.central.components;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.handler.listeners.AlarmListener;
import com.github.soshibby.volvo.canbus.types.AlarmMessage;

public class Alarm {
	private List<AlarmListener> listeners = new ArrayList<AlarmListener>();
	private AlarmMessage previousMessage;

	public void handleMessage(AlarmMessage message) {
		if (hasAlarmChanged(message)) {
			for (AlarmListener listener : this.listeners) {
				listener.onAlarmTriggeredChanged(message.isAlarmTriggered());
			}
		}
		
		previousMessage = message;
	}

	private boolean hasAlarmChanged(AlarmMessage alarmMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isAlarmTriggered() != alarmMessage.isAlarmTriggered();
	}

	public void addListener(AlarmListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(AlarmListener listener) {
		this.listeners.remove(listener);
	}
}
