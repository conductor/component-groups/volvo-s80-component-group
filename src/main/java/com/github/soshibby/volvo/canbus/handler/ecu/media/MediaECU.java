package com.github.soshibby.volvo.canbus.handler.ecu.media;

import javax.inject.Named;

import com.github.soshibby.volvo.canbus.handler.ecu.media.components.Phone;
import com.github.soshibby.volvo.canbus.handler.ecu.media.components.Radio;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.PhoneMessage;
import com.github.soshibby.volvo.canbus.types.RadioMessage;

@Named
public class MediaECU {
	Phone phone = new Phone();
	Radio radio = new Radio();
	
	public void handleMessage(CanMessage message) {
		if (message instanceof PhoneMessage) {
			phone.handleMessage((PhoneMessage) message);
		} else if (message instanceof RadioMessage) {
			radio.handleMessage((RadioMessage) message);
		}
	}

	public Phone getPhone() {
		return phone;
	}
	
	public Radio getRadio() {
		return radio;
	}
}
