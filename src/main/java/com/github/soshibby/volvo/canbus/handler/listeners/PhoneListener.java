package com.github.soshibby.volvo.canbus.handler.listeners;

public interface PhoneListener {
	public void onAnswerCallChanged(boolean anwerCall);

	public void onRejectCallChanged(boolean rejectCall);
}
