package com.github.soshibby.volvo.canbus.handler.listeners;

import com.github.soshibby.volvo.canbus.types.enums.WindowPosition;

public interface WindowListener {
	public void onWindowPositionChanged(WindowPosition windowPosition);
}
