package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class SpeedometerMessage extends CanMessage {
	private int currentSpeed;

	public SpeedometerMessage(ECU ecu) {
		super(ecu);
	}

	public int getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed(int currentSpeed) {
		this.currentSpeed = currentSpeed;
	}
}
