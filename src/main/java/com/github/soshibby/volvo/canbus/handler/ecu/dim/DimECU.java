package com.github.soshibby.volvo.canbus.handler.ecu.dim;

import javax.inject.Named;

import com.github.soshibby.volvo.canbus.handler.ecu.dim.components.Dim;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.DimMessage;

@Named
public class DimECU {
	private Dim dim;
	
	public void handleMessage(CanMessage message) {
		if (message instanceof DimMessage) {
			dim.handleMessage((DimMessage) message);
		}
	}
	
	public Dim getDim() {
		return dim;
	}

}
