package com.github.soshibby.volvo.canbus.handler.ecu.passengerdoor;

import javax.inject.Named;

import com.github.soshibby.volvo.canbus.handler.ecu.driverdoor.components.Lock;
import com.github.soshibby.volvo.canbus.handler.ecu.driverdoor.components.Window;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.LockMessage;
import com.github.soshibby.volvo.canbus.types.WindowMessage;

@Named
public class PassengerDoorECU {
	private Lock lock = new Lock();
	private Window window = new Window();
	
	public void handleMessage(CanMessage message) {
		if (message instanceof LockMessage) {
			lock.handleMessage((LockMessage) message);
		} else if(message instanceof WindowMessage) {
			window.handleMessage((WindowMessage) message);
		}
	}

	public Lock getLock() {
		return lock;
	}
	
	public Window getWindow() {
		return window;
	}
}
