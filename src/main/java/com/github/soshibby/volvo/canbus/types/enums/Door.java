package com.github.soshibby.volvo.canbus.types.enums;

public enum Door {
	DRIVER,
	PASSENGER,
	REAR_LEFT,
	REAR_RIGHT
}
