package com.github.soshibby.volvo.canbus.parser.ecu;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.soshibby.volvo.canbus.parser.ICanMessageParser;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.PhoneMessage;
import com.github.soshibby.volvo.canbus.types.RadioMessage;
import com.github.soshibby.volvo.canbus.types.enums.ECU;
import com.github.soshibby.volvo.canbus.utils.ByteUtil;

import de.entropia.can.CanSocket.CanFrame;

public class MediaECUParser implements ICanMessageParser {
	private Logger log = LogManager.getLogger(MediaECUParser.class.getName());

	public List<CanMessage> parse(CanFrame frame) {
		byte[] data = frame.getData();
		List<CanMessage> messages = new ArrayList<CanMessage>();

		messages.add(parseRadioButtonMessage(data));
		messages.add(parsePhoneButtonMessage(data));

		return messages;
	}

	private CanMessage parseRadioButtonMessage(byte[] data) {
		RadioMessage radio = new RadioMessage(ECU.MEDIA);

		byte mediaButtons = ByteUtil.getLowNibble(data[7]);

		switch (mediaButtons) {
		case 0xF:
			// No button was pressed
			break;
		case 0xB:
			radio.setDecreaseVolume(true);
			break;
		case 0x7:
			radio.setIncreaseVolume(true);
			break;
		case 0xE:
			radio.setPreviousTrack(true);
		case 0xD:
			radio.setNextTrack(true);
		default:
			log.warn("Received an unknown radio button code from the canbus.");
		}

		return radio;
	}

	private CanMessage parsePhoneButtonMessage(byte[] data) {
		PhoneMessage phone = new PhoneMessage(ECU.MEDIA);
		byte phoneButtons = ByteUtil.getHighNibble(data[7]);

		switch (phoneButtons) {
		case 0x7:
			// No button was pressed
		  break;
		case 0x6:
			phone.setAnswerCall(true);
			break;
		case 0x5:
			phone.setRejectCall(true);
			break;
		default:
			log.info("Received an unknown phone button code from the canbus.");
		}

		return phone;
	}
}
