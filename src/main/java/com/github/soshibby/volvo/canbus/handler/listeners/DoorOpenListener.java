package com.github.soshibby.volvo.canbus.handler.listeners;

import com.github.soshibby.volvo.canbus.types.DoorMessage;

public interface DoorOpenListener {
	public void onDriverDoorOpenChanged(boolean isDoorOpen);

	public void onPassengerDoorOpenChanged(boolean isDoorOpen);

	public void onRearLeftDoorOpenChanged(boolean isDoorOpen);

	public void onRearRightDoorOpenChanged(boolean isDoorOpen);
}
