package com.github.soshibby.volvo.canbus.handler.listeners;

import com.github.soshibby.volvo.canbus.types.AlarmMessage;

public interface AlarmListener {
	public void onAlarmTriggeredChanged(boolean isAlarmTriggered);
}
