package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class PhoneMessage extends CanMessage {
	private boolean answerCall;
	private boolean rejectCall;

	public PhoneMessage(ECU ecu) {
		super(ecu);
	}

	public boolean isAnswerCall() {
		return answerCall;
	}

	public void setAnswerCall(boolean answerCall) {
		this.answerCall = answerCall;
	}

	public boolean isRejectCall() {
		return rejectCall;
	}

	public void setRejectCall(boolean rejectCall) {
		this.rejectCall = rejectCall;
	}
}
