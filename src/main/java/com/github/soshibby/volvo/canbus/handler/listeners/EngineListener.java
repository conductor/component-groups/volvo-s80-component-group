package com.github.soshibby.volvo.canbus.handler.listeners;

public interface EngineListener {
	public void onRPMChanged(int rpm);
}
