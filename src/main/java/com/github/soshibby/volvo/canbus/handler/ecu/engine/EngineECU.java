package com.github.soshibby.volvo.canbus.handler.ecu.engine;

import javax.inject.Named;

import com.github.soshibby.volvo.canbus.handler.ecu.engine.components.Engine;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.EngineMessage;

@Named
public class EngineECU {
	private Engine engine = new Engine();
	
	public void handleMessage(CanMessage message) {
		if (message instanceof EngineMessage) {
			engine.handleMessage((EngineMessage) message);
		}
	}
	
	public Engine getEngine() {
		return engine;
	}
	
}
