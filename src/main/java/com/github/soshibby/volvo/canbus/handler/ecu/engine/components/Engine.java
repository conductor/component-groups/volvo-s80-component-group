package com.github.soshibby.volvo.canbus.handler.ecu.engine.components;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.handler.listeners.EngineListener;
import com.github.soshibby.volvo.canbus.types.EngineMessage;

public class Engine {
	private List<EngineListener> listeners = new ArrayList<EngineListener>();
	private EngineMessage previousMessage;

	public void handleMessage(EngineMessage message) {
		if (hasRPMChanged(message)) {
			for (EngineListener listener : this.listeners) {
				listener.onRPMChanged(message.getRpm());
			}
		}
		
		previousMessage = message;
	}

	private boolean hasRPMChanged(EngineMessage engineMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.getRpm() != engineMessage.getRpm();
	}

	public void addListener(EngineListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(EngineListener listener) {
		this.listeners.remove(listener);
	}
}
