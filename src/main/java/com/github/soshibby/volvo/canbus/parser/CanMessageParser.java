package com.github.soshibby.volvo.canbus.parser;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.types.CanMessage;

import de.entropia.can.CanSocket.CanFrame;

public class CanMessageParser {
  private ParserFactory parserFactory = new ParserFactory();

  public List<CanMessage> parse(CanFrame frame) {
    int ecuId = frame.getCanId().isSetEFFSFF() ? frame.getCanId().getCanId_EFF() : frame.getCanId().getCanId_SFF();
    ICanMessageParser parser = this.parserFactory.create(ecuId);

    if (parser == null) {
      return new ArrayList<CanMessage>();
    }

    return parser.parse(frame);
  }
}
