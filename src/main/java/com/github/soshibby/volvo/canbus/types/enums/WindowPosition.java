package com.github.soshibby.volvo.canbus.types.enums;

public enum WindowPosition {
	UP,
	ALMOST_UP,
	ALMOST_DOWN,
	DOWN,
	UNKNOWN
}
