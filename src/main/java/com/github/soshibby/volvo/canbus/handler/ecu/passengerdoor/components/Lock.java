package com.github.soshibby.volvo.canbus.handler.ecu.passengerdoor.components;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.handler.listeners.LockListener;
import com.github.soshibby.volvo.canbus.types.LockMessage;

public class Lock {
	private List<LockListener> listeners = new ArrayList<LockListener>();
	private LockMessage previousMessage;

	public void handleMessage(LockMessage message) {
		if (hasPassengerDoorLockChanged(message)) {
			for (LockListener listener : this.listeners) {
				listener.onLockStatusChanged(message.isLocked());
			}
		}
		
		previousMessage = message;
	}

	private boolean hasPassengerDoorLockChanged(LockMessage lockMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isLocked() != lockMessage.isLocked();
	}

	public void addListener(LockListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(LockListener listener) {
		this.listeners.remove(listener);
	}
}
