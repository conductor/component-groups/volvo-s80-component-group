package com.github.soshibby.volvo.canbus.handler.ecu.media.components;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.handler.listeners.RadioListener;
import com.github.soshibby.volvo.canbus.types.RadioMessage;

public class Radio {
	private List<RadioListener> listeners = new ArrayList<RadioListener>();
	private RadioMessage previousMessage;

	public void handleMessage(RadioMessage message) {
		if (hasNextTrackChanged(message)) {
			for (RadioListener listener : this.listeners) {
				listener.onNextTrackPressed(message.isNextTrack());
			}
		}
		
		if (hasPreviousTrackChanged(message)) {
			for (RadioListener listener : this.listeners) {
				listener.onPreviousTrackPressed(message.isPreviousTrack());
			}
		}
		
		if (hasIncreaseVolumeChanged(message)) {
			for (RadioListener listener : this.listeners) {
				listener.onIncreaseVolumePressed(message.isIncreaseVolume());
			}
		}
		
		if (hasDecreaseVolumeChanged(message)) {
			for (RadioListener listener : this.listeners) {
				listener.onDecreaseVolumePressed(message.isDecreaseVolume());
			}
		}
		
		previousMessage = message;
	}

	private boolean hasNextTrackChanged(RadioMessage radioMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isNextTrack() != radioMessage.isNextTrack();
	}
	
	private boolean hasPreviousTrackChanged(RadioMessage radioMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isPreviousTrack() != radioMessage.isPreviousTrack();
	}
	
	private boolean hasIncreaseVolumeChanged(RadioMessage radioMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isIncreaseVolume() != radioMessage.isIncreaseVolume();
	}

	private boolean hasDecreaseVolumeChanged(RadioMessage radioMessage) {
		if (previousMessage == null) {
			return true;
		}

		return previousMessage.isDecreaseVolume() != radioMessage.isDecreaseVolume();
	}
	
	public void addListener(RadioListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(RadioListener listener) {
		this.listeners.remove(listener);
	}
}
