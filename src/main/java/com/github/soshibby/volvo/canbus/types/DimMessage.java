package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class DimMessage extends CanMessage {
	private int counter;
	
	public DimMessage(ECU ecu) {
		super(ecu);
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
}
