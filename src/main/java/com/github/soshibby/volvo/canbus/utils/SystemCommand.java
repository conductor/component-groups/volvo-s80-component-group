package com.github.soshibby.volvo.canbus.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SystemCommand {
	public static String exec(String cmd) throws Exception {
		Process process = Runtime.getRuntime().exec(cmd);

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		String input = "";
		String line;
		while ((line = stdInput.readLine()) != null) {
			input += line;
		}

		String error = "";
		while ((line = stdError.readLine()) != null) {
			error += line;
		}

		if (error.length() != 0) {
			throw new Exception("Error when running system command '" + cmd + "', error message: " + error);
		}

		return input;
	}
}
