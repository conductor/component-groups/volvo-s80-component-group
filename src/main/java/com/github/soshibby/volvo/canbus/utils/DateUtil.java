package com.github.soshibby.volvo.canbus.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

  public static Date addDays(Date date, int days) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.DATE, days); // minus number would decrement the days
    return cal.getTime();
  }

  public static Date addHours(Date date, int hours) {
    Calendar cal = Calendar.getInstance(); // creates calendar
    cal.setTime(new Date()); // sets calendar time/date
    cal.add(Calendar.HOUR_OF_DAY, hours);
    return cal.getTime(); // returns new date object, one hour in the future
  }

  public static Date addMinutes(Date date, int minutes) {
    Calendar cal = Calendar.getInstance(); // creates calendar
    cal.setTime(new Date()); // sets calendar time/date
    cal.add(Calendar.MINUTE, minutes);
    return cal.getTime(); // returns new date object, one hour in the future
  }

  public static Date addSeconds(Date date, int seconds) {
    Calendar cal = Calendar.getInstance(); // creates calendar
    cal.setTime(new Date()); // sets calendar time/date
    cal.add(Calendar.SECOND, seconds);
    return cal.getTime(); // returns new date object, one hour in the future
  }

}
