package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class DoorMessage extends CanMessage {
	private boolean isDriverDoorOpen;
	private boolean isPassengerDoorOpen;
	private boolean isRearLeftDoorOpen;
	private boolean isRearRightDoorOpen;
	private int counter;

	public DoorMessage(ECU ecu) {
		super(ecu);
	}

	public boolean isDriverDoorOpen() {
		return isDriverDoorOpen;
	}

	public void setDriverDoorOpen(boolean isDriverDoorOpen) {
		this.isDriverDoorOpen = isDriverDoorOpen;
	}

	public boolean isPassengerDoorOpen() {
		return isPassengerDoorOpen;
	}

	public void setPassengerDoorOpen(boolean isPassengerDoorOpen) {
		this.isPassengerDoorOpen = isPassengerDoorOpen;
	}

	public boolean isRearLeftDoorOpen() {
		return isRearLeftDoorOpen;
	}

	public void setRearLeftDoorOpen(boolean isRearLeftDoorOpen) {
		this.isRearLeftDoorOpen = isRearLeftDoorOpen;
	}

	public boolean isRearRightDoorOpen() {
		return isRearRightDoorOpen;
	}

	public void setRearRightDoorOpen(boolean isRearRightDoorOpen) {
		this.isRearRightDoorOpen = isRearRightDoorOpen;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
}
