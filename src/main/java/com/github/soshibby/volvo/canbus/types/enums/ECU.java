package com.github.soshibby.volvo.canbus.types.enums;

public enum ECU {
	CENTRAL, 
	UPPER, 
	MEDIA,
	DRIVER_DOOR,
	PASSENGER_DOOR,
	ENGINE,
	DIM
}
