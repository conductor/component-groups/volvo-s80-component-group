package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class AlarmMessage extends CanMessage {
	private boolean isAlarmTriggered;

	public AlarmMessage(ECU ecu) {
		super(ecu);
	}

	public boolean isAlarmTriggered() {
		return isAlarmTriggered;
	}

	public void setAlarmTriggered(boolean isAlarmTriggered) {
		this.isAlarmTriggered = isAlarmTriggered;
	}
}
