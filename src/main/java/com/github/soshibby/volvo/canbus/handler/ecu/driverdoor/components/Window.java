package com.github.soshibby.volvo.canbus.handler.ecu.driverdoor.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.soshibby.volvo.canbus.handler.listeners.WindowListener;
import com.github.soshibby.volvo.canbus.sender.CanSender;
import com.github.soshibby.volvo.canbus.types.WindowMessage;

public class Window {
	private Logger log = LogManager.getLogger(Window.class.getName());
	private List<WindowListener> listeners = new ArrayList<WindowListener>();
	private WindowMessage previousMessage;
	private CanSender canbusSender;

	public void handleMessage(WindowMessage message) {
		if (hasDriverWindowChanged(message)) {
			for (WindowListener listener : this.listeners) {
				listener.onWindowPositionChanged(message.getPosition());
			}
		}

		previousMessage = message;
	}

	private boolean hasDriverWindowChanged(WindowMessage windowMessage) {
		if (previousMessage == null) {
			return true;
		}

		return !previousMessage.getPosition().equals(windowMessage.getPosition());
	}

	public void moveDown() {
		try {
			canbusSender.send(0x000FFFFE, new byte[] { (byte) 0x8F, 0x43, (byte) 0xB0, 0x10, 0x01, 0x01, (byte) 0xFC, 0x02 });
			canbusSender.send(0x000FFFFE, new byte[] { 0x4B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
		} catch (Exception e) {
			log.error("Failed to send message to canbus.", e);
		}
	}

	public void moveUp() {
		try {
			canbusSender.send(0x000FFFFE, new byte[] { (byte) 0x8F, 0x43, (byte) 0xB0, 0x10, 0x01, 0x01, (byte) 0xFC, 0x01 });
			canbusSender.send(0x000FFFFE, new byte[] { 0x4B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
		} catch (Exception e) {
			log.error("Failed to send message to canbus.", e);
		}
	}

	public void stop() {
		try {
			canbusSender.send(0x000FFFFE, new byte[] { (byte) 0xCC, 0x43, (byte) 0xB0, 0x10, 0x00, 0x00, 0x00, 0x00 });
		} catch (Exception e) {
			log.error("Failed to send message to canbus.", e);
		}
	}

	public void addListener(WindowListener listener) {
		this.listeners.add(listener);
	}

	public void removeListener(WindowListener listener) {
		this.listeners.remove(listener);
	}
}

