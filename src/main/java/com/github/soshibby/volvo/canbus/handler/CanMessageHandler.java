package com.github.soshibby.volvo.canbus.handler;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.github.soshibby.volvo.canbus.handler.ecu.central.CentralECU;
import com.github.soshibby.volvo.canbus.handler.ecu.driverdoor.DriverDoorECU;
import com.github.soshibby.volvo.canbus.handler.ecu.engine.EngineECU;
import com.github.soshibby.volvo.canbus.handler.ecu.media.MediaECU;
import com.github.soshibby.volvo.canbus.handler.ecu.passengerdoor.PassengerDoorECU;
import com.github.soshibby.volvo.canbus.types.CanMessage;

@Named
public class CanMessageHandler {
	
	@Inject
	private CentralECU centralECU;
	
	@Inject
	private MediaECU mediaECU;
	
	@Inject
	private PassengerDoorECU passengerDoorECU;
	
	@Inject
	private DriverDoorECU driverDoorECU;
	
	@Inject
	private EngineECU engineECU;
	
	public void handle(List<CanMessage> messages) {
		for (CanMessage message : messages) {
			switch (message.getECU()) {
			case CENTRAL:
				centralECU.handleMessage(message);
				break;
			case MEDIA:
				mediaECU.handleMessage(message);
				break;
			case DRIVER_DOOR:
				driverDoorECU.handleMessage(message);
			case PASSENGER_DOOR:
				passengerDoorECU.handleMessage(message);
				break;
			case ENGINE:
				engineECU.handleMessage(message);
			}
		}
	}
}
