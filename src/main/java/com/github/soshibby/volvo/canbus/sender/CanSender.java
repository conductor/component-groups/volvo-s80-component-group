package com.github.soshibby.volvo.canbus.sender;

import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.soshibby.volvo.canbus.utils.SystemCommand;

@Named
public class CanSender {

  private String canInterface = "can0";
	private Logger log = LogManager.getLogger(CanSender.class.getName());

	public CanSender() {

	}

	public CanSender(String canInterface) {
		this.canInterface = canInterface;
	}

	public void send(int id, byte[] data) throws Exception {
		String strData = "";
		for (byte dataByte : data) {
			strData += String.format("%02X", dataByte);
		}

		String message = String.format("cansend %s %08X#%s", canInterface, id, strData);

		log.info("Sending message to canbus: '{}'", message);

		SystemCommand.exec(message);
	}
}
