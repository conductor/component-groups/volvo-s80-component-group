package com.github.soshibby.volvo.canbus.receiver;

public interface ICanReceiver {
	public void listen();
}
