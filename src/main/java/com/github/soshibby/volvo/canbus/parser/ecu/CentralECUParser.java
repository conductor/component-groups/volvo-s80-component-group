package com.github.soshibby.volvo.canbus.parser.ecu;

import java.util.ArrayList;
import java.util.List;

import com.github.soshibby.volvo.canbus.parser.ICanMessageParser;
import com.github.soshibby.volvo.canbus.types.AlarmMessage;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.DoorMessage;
import com.github.soshibby.volvo.canbus.types.SpeedometerMessage;
import com.github.soshibby.volvo.canbus.types.enums.ECU;
import com.github.soshibby.volvo.canbus.utils.ByteUtil;

import de.entropia.can.CanSocket.CanFrame;

public class CentralECUParser implements ICanMessageParser {

	public List<CanMessage> parse(CanFrame frame) {
		byte[] data = frame.getData();
		List<CanMessage> messages = new ArrayList<CanMessage>();

		messages.add(parseDoorMessage(data));
		messages.add(parseAlarmMessage(data));
		messages.add(parseSpeedometerMessage(data));

		return messages;
	}

	public DoorMessage parseDoorMessage(byte[] data) {
		DoorMessage door = new DoorMessage(ECU.CENTRAL);
		byte counter = ByteUtil.getHighNibble(data[0]);
		door.setCounter(counter);
		byte doorPositions = ByteUtil.getLowNibble(data[5]);

        if (ByteUtil.isBitSet(doorPositions, 0)) {
            door.setDriverDoorOpen(true);
        } else {
        	door.setDriverDoorOpen(false);
        }

        if (ByteUtil.isBitSet(doorPositions, 1)) {
            door.setPassengerDoorOpen(true);
        } else {
            door.setPassengerDoorOpen(false);
        }

        if (ByteUtil.isBitSet(doorPositions, 2)) {
            door.setRearLeftDoorOpen(true);
        } else {
            door.setRearLeftDoorOpen(false);
        }

        if (ByteUtil.isBitSet(doorPositions, 3)) {
        	door.setRearRightDoorOpen(true);
        } else {
        	door.setRearRightDoorOpen(false);
        }

        return door;
	}

	public SpeedometerMessage parseSpeedometerMessage(byte[] data) {
		SpeedometerMessage speedometer = new SpeedometerMessage(ECU.CENTRAL);

		if(ByteUtil.getHighNibble(data[6]) == 0x3) {
		  data[6] -= 0x38;  // When the car is on and the speed is 0d, the can message is 38h
		}else{
		  data[6] -= 0x8;   // When the car is off and the speed is 0d, the can message is 08h
		}

		int speed = data[6] * 0x100;
		speed += data[7];

		speedometer.setCurrentSpeed(speed);

		return speedometer;
	}

	public AlarmMessage parseAlarmMessage(byte[] data) {
		AlarmMessage alarm = new AlarmMessage(ECU.CENTRAL);

		//80 50 00 33 17 00 00 00 && 01 50 00 3B 17 00 00 00
    	boolean triggered = data[3] == 0x33 || data[3] == 0x3B;
		alarm.setAlarmTriggered(triggered);

        return alarm;
	}

}
