package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class EngineMessage extends CanMessage {
	private int rpm;
	
	public EngineMessage(ECU ecu) {
		super(ecu);
	}

	public int getRpm() {
		return rpm;
	}

	public void setRpm(int rpm) {
		this.rpm = rpm;
	}

}
