package com.github.soshibby.volvo.canbus.handler.listeners;

import com.github.soshibby.volvo.canbus.types.SpeedometerMessage;

public interface SpeedometerListener {
	public void onCurrentSpeedChanged(int currentSpeed);
}
