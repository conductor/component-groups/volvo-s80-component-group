package com.github.soshibby.volvo.canbus.parser;

import java.util.HashMap;
import java.util.Map;

import com.github.soshibby.volvo.canbus.parser.ecu.CentralECUParser;
import com.github.soshibby.volvo.canbus.parser.ecu.DimECUParser;
import com.github.soshibby.volvo.canbus.parser.ecu.DriverDoorECUParser;
import com.github.soshibby.volvo.canbus.parser.ecu.EngineECUParser;
import com.github.soshibby.volvo.canbus.parser.ecu.MediaECUParser;
import com.github.soshibby.volvo.canbus.parser.ecu.PassengerDoorECUParser;

public class ParserFactory {
	private static final Map<Integer, ICanMessageParser> parsers;
	
    static {
    	parsers = new HashMap<Integer, ICanMessageParser>();
    	parsers.put(0x00403FFC, new CentralECUParser());
    	parsers.put(0x00100066, new MediaECUParser());
    	parsers.put(0x00903082, new DriverDoorECUParser());
    	parsers.put(0x01301008, new EngineECUParser());
    	parsers.put(0x00B00006, new DimECUParser());
    	parsers.put(0x00A01012, new PassengerDoorECUParser());
    }
	
	public ICanMessageParser create(int ecuId) {
		return parsers.get(ecuId);
	}
}
