package com.github.soshibby.volvo.canbus.parser.ecu;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.soshibby.volvo.canbus.parser.ICanMessageParser;
import com.github.soshibby.volvo.canbus.types.CanMessage;
import com.github.soshibby.volvo.canbus.types.LockMessage;
import com.github.soshibby.volvo.canbus.types.WindowMessage;
import com.github.soshibby.volvo.canbus.types.enums.Door;
import com.github.soshibby.volvo.canbus.types.enums.ECU;
import com.github.soshibby.volvo.canbus.types.enums.WindowPosition;
import com.github.soshibby.volvo.canbus.utils.ByteUtil;

import de.entropia.can.CanSocket.CanFrame;

public class DriverDoorECUParser implements ICanMessageParser {
  private Logger log = LogManager.getLogger(DriverDoorECUParser.class.getName());

  public List<CanMessage> parse(CanFrame frame) {
    byte[] data = frame.getData();
    List<CanMessage> messages = new ArrayList<CanMessage>();

    messages.add(parseWindowMessage(data));
    messages.add(parseLockMessage(data));

    return messages;
  }

  public LockMessage parseLockMessage(byte[] data) {
    LockMessage lock = new LockMessage(ECU.DRIVER_DOOR);
    lock.setDoor(Door.DRIVER);
    byte driverLock = ByteUtil.getHighNibble(data[5]);

    switch (driverLock) {
    case 0x0:
    case 0x8:
      lock.setLocked(false);
      break;
    case 0x1:
    case 0x3:
    case 0x9:
    case 0xB:
      lock.setLocked(true);
      break;
    default:
      log.warn("Invalid lock message received from canbus. Recieved {} while expecting the value to be 1 or 0.", driverLock);
    }

    return lock;
  }

  public WindowMessage parseWindowMessage(byte[] data) {
    WindowMessage window = new WindowMessage(ECU.DRIVER_DOOR);
    window.setDoor(Door.DRIVER);

    byte driverWindowPosition = ByteUtil.getLowNibble(data[4]);

    switch (driverWindowPosition) {
    case 0x9:
      window.setPosition(WindowPosition.UP);
      break;
    case 0xA:
      window.setPosition(WindowPosition.ALMOST_UP);
      break;
    case 0xE:
      window.setPosition(WindowPosition.ALMOST_DOWN);
      break;
    case 0xF:
      window.setPosition(WindowPosition.DOWN);
      break;
    default:
      log.error("Unknown window position message.");
      window.setPosition(WindowPosition.UNKNOWN);
    }

    return window;
  }
}
