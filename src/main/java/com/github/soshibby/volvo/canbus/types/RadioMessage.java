package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public class RadioMessage extends CanMessage {
	private boolean nextTrack;
	private boolean previousTrack;
	private boolean increaseVolume;
	private boolean decreaseVolume;

	public RadioMessage(ECU ecu) {
		super(ecu);
	}

	public boolean isNextTrack() {
		return nextTrack;
	}

	public void setNextTrack(boolean nextTrack) {
		this.nextTrack = nextTrack;
	}

	public boolean isPreviousTrack() {
		return previousTrack;
	}

	public void setPreviousTrack(boolean previousTrack) {
		this.previousTrack = previousTrack;
	}

	public boolean isIncreaseVolume() {
		return increaseVolume;
	}

	public void setIncreaseVolume(boolean increaseVolume) {
		this.increaseVolume = increaseVolume;
	}

	public boolean isDecreaseVolume() {
		return decreaseVolume;
	}

	public void setDecreaseVolume(boolean decreaseVolume) {
		this.decreaseVolume = decreaseVolume;
	}

}
