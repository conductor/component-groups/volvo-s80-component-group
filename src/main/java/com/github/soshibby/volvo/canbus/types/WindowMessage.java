package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.Door;
import com.github.soshibby.volvo.canbus.types.enums.ECU;
import com.github.soshibby.volvo.canbus.types.enums.WindowPosition;

public class WindowMessage extends CanMessage {
	private WindowPosition position;
	private Door door;
	
	public WindowMessage(ECU ecu) {
		super(ecu);
	}

	public WindowPosition getPosition() {
		return position;
	}

	public void setPosition(WindowPosition position) {
		this.position = position;
	}

	public Door getDoor() {
		return door;
	}

	public void setDoor(Door door) {
		this.door = door;
	}
	

}
