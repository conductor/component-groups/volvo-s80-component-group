package com.github.soshibby.volvo.canbus.types;

import com.github.soshibby.volvo.canbus.types.enums.ECU;

public abstract class CanMessage {
	private ECU ecu;

	public CanMessage(ECU ecu) {
		this.ecu = ecu;
	}

	public ECU getECU() {
		return this.ecu;
	}
}
