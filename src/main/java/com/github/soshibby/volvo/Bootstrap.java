package com.github.soshibby.volvo;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.bootstrap.IComponentGroupBootstrap;
import org.conductor.component.annotations.ComponentGroupBootstrap;
import org.conductor.component.types.IComponent;
import org.conductor.database.Database;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.github.soshibby.volvo.canbus.handler.ecu.central.CentralECU;
import com.github.soshibby.volvo.canbus.handler.ecu.dim.DimECU;
import com.github.soshibby.volvo.canbus.handler.ecu.driverdoor.DriverDoorECU;
import com.github.soshibby.volvo.canbus.handler.ecu.engine.EngineECU;
import com.github.soshibby.volvo.canbus.handler.ecu.media.MediaECU;
import com.github.soshibby.volvo.canbus.handler.ecu.passengerdoor.PassengerDoorECU;
import com.github.soshibby.volvo.canbus.receiver.CanReceiver;
import com.github.soshibby.volvo.canbus.receiver.ICanReceiver;
import com.github.soshibby.volvo.components.Alarm;
import com.github.soshibby.volvo.components.Dim;
import com.github.soshibby.volvo.components.DriverDoor;
import com.github.soshibby.volvo.components.DriverWindow;
import com.github.soshibby.volvo.components.PassengerDoor;
import com.github.soshibby.volvo.components.PassengerWindow;
import com.github.soshibby.volvo.components.Phone;
import com.github.soshibby.volvo.components.Radio;
import com.github.soshibby.volvo.components.RearLeftDoor;
import com.github.soshibby.volvo.components.RearRightDoor;
import com.github.soshibby.volvo.components.Speedometer;

@ComponentGroupBootstrap
@Named
public class Bootstrap implements IComponentGroupBootstrap {
	private static Logger log = LogManager.getLogger(Bootstrap.class.getName());

	@Inject
	@Qualifier("canReceiver")
	private ICanReceiver canReceiver;

	@Inject
	private CentralECU centralECU;

	@Inject
	private DimECU dimECU;

	@Inject
	private DriverDoorECU driverDoorECU;

	@Inject
	private PassengerDoorECU passengerDoorECU;

	@Inject
	private EngineECU engineECU;

	@Inject
	private MediaECU mediaECU;

	public Bootstrap() {

	}

	@Override
	public Map<String, IComponent> start(Database database, Map<String, Object> componentGroupOptions, Map<String, Map<String, Object>> componentsOptions) throws Exception {
		log.info("Starting volvo component group");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Spring-AutoScan2.xml");
		Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
		Map<String, IComponent> components = bootstrap.getComponents(database, componentsOptions);
		bootstrap.startCanBus();
		context.close();
		return components;
	}

	public Map<String, IComponent> getComponents(Database database, Map<String, Map<String, Object>> componentsOptions) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Map<String, IComponent> components = new HashMap<String, IComponent>();

		Alarm alarm = new Alarm(database, componentsOptions.get("alarm"), this.centralECU);
		Dim dim = new Dim(database, componentsOptions.get("dim"), dimECU);
		DriverDoor driverDoor = new DriverDoor(database, componentsOptions.get("driverDoor"), centralECU, driverDoorECU);
		DriverWindow driverWindow = new DriverWindow(database, componentsOptions.get("driverWindow"), driverDoorECU);
		PassengerDoor passengerDoor = new PassengerDoor(database, componentsOptions.get("passengerDoor"), centralECU, passengerDoorECU);
		PassengerWindow passengerWindow = new PassengerWindow(database, componentsOptions.get("passengerWindow"), passengerDoorECU);
		Phone phone = new Phone(database, componentsOptions.get("phone"), mediaECU);
		Radio radio = new Radio(database, componentsOptions.get("radio"), mediaECU);
		RearLeftDoor rearLeftDoor = new RearLeftDoor(database, componentsOptions.get("rearLeftDoor"), centralECU);
		RearRightDoor rearRightDoor = new RearRightDoor(database, componentsOptions.get("rearRightDoor"), centralECU);
		Speedometer speedometer = new Speedometer(database, componentsOptions.get("speedometer"), centralECU);

		components.put("Alarm", alarm);
		components.put("Dim", dim);
		components.put("DriverDoor", driverDoor);
		components.put("DriverWindow", driverWindow);
		components.put("PassengerDoor", passengerDoor);
		components.put("PassengerWindow", passengerWindow);
		components.put("Phone", phone);
		components.put("Radio", radio);
		components.put("RearLeftDoor", rearLeftDoor);
		components.put("RearRightDoor", rearRightDoor);
		components.put("Speedometer", speedometer);

		return components;
	}

	private void startCanBus() {
		this.canReceiver.listen();
	}

}
