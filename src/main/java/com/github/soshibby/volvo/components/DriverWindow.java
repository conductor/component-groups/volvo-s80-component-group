package com.github.soshibby.volvo.components;

import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.driverdoor.DriverDoorECU;
import com.github.soshibby.volvo.canbus.handler.listeners.WindowListener;
import com.github.soshibby.volvo.canbus.types.enums.WindowPosition;

@org.conductor.component.annotations.Component(type = "window")
public class DriverWindow extends Component {
	private static Logger log = LogManager.getLogger(DriverWindow.class.getName());
	private DriverDoorECU driverDoorECU;

	public DriverWindow(Database database, Map<String, Object> options, DriverDoorECU driverDoorECU) {
		super(database, options);
		this.driverDoorECU = driverDoorECU;
		this.driverDoorECU.getWindow().addListener(windowListener);
	}

	@Property(enums = { "up", "almostUp", "almostDown", "down" }, initialValue = "up")
	public void setPosition(String position) throws Exception {
	  log.info("Moving driver window {}.", position);


	  switch (position) {
		case "up":
		case "almostUp":
			this.driverDoorECU.getWindow().moveUp();
			break;
		case "down":
		case "almostDown":
			this.driverDoorECU.getWindow().moveDown();
			break;
		}
	}

	private WindowListener windowListener = new WindowListener() {

		public void onWindowPositionChanged(WindowPosition windowPosition) {
			try {
				String position = "";

				switch (windowPosition) {
				case UP:
					position = "up";
					break;
				case ALMOST_UP:
					position = "almostUp";
					break;
				case ALMOST_DOWN:
					position = "almostDown";
					break;
				case DOWN:
					position = "down";
					break;
				}

				updatePropertyValue("position", position);
			} catch (Exception e) {
				log.error("Failed to update property value.", e);
				reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
			}
		}

	};

}
