package com.github.soshibby.volvo.components;

import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.central.CentralECU;
import com.github.soshibby.volvo.canbus.handler.ecu.passengerdoor.PassengerDoorECU;
import com.github.soshibby.volvo.canbus.handler.listeners.DoorOpenListener;
import com.github.soshibby.volvo.canbus.handler.listeners.LockListener;

@org.conductor.component.annotations.Component(type = "door")
public class PassengerDoor extends Component {
	private static Logger log = LogManager.getLogger(PassengerDoor.class.getName());
	private CentralECU centralECU;

	public PassengerDoor(Database database, Map<String, Object> options, CentralECU centralECU, PassengerDoorECU passengerDoorECU) {
		super(database, options);
		this.centralECU = centralECU;
		this.centralECU.getDoors().addListener(doorOpenListener);
		passengerDoorECU.getLock().addListener(lockListener);
	}

	@Property(initialValue = "false")
	public void setOpen(Boolean open) throws Exception {
		throw new Exception("Can't set property 'open' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Property(initialValue = "false")
	public void setLocked(Boolean locked) throws Exception {
		if (locked) {
			this.centralECU.getDoors().lockDoors();
		} else {
			this.centralECU.getDoors().unlockDoors();
		}
	}

	private DoorOpenListener doorOpenListener = new DoorOpenListener() {

		public void onDriverDoorOpenChanged(boolean isDoorOpen) {

		}

		public void onPassengerDoorOpenChanged(boolean isDoorOpen) {
		  log.info("Passenger door opened changed to {}.", isDoorOpen);

			try {
				updatePropertyValue("open", isDoorOpen);
			} catch (Exception e) {
				log.error("Failed to update property value.");
				reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
			}
		}

		public void onRearLeftDoorOpenChanged(boolean isDoorOpen) {

		}

		public void onRearRightDoorOpenChanged(boolean isDoorOpen) {

		}
	};

	private LockListener lockListener = new LockListener() {

		public void onLockStatusChanged(boolean isLocked) {
		  log.info("Passenger door locked changed to {}.", isLocked);

			try {
				updatePropertyValue("locked", isLocked);
			} catch (Exception e) {
				log.error("Failed to update property value.");
				reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
			}
		}

	};
}
