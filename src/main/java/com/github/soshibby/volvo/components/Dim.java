package com.github.soshibby.volvo.components;

import java.util.Map;

import org.conductor.component.types.Component;
import org.conductor.database.Database;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.soshibby.volvo.canbus.handler.ecu.dim.DimECU;

@org.conductor.component.annotations.Component(type = "dim")
public class Dim extends Component {
	private DimECU dimECU;
	
	public Dim(Database database, Map<String, Object> options, DimECU dimECU) {
		super(database, options);
		this.dimECU = dimECU;
	}
	
	@Autowired
	public void setMessage(String message) {
		this.dimECU.getDim().displayText(message);
	}

}
