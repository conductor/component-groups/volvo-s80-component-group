package com.github.soshibby.volvo.components;

import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.central.CentralECU;
import com.github.soshibby.volvo.canbus.handler.listeners.DoorOpenListener;

@org.conductor.component.annotations.Component(type = "door")
public class RearLeftDoor extends Component implements DoorOpenListener {
	private static Logger log = LogManager.getLogger(RearLeftDoor.class.getName());

	public RearLeftDoor(Database database, Map<String, Object> options, CentralECU ecu) {
		super(database, options);
		ecu.getDoors().addListener(this);
	}

	@Property(initialValue = "false")
	public void setOpen(Boolean open) throws Exception {
		throw new Exception("Can't set property 'open' of component '" + this.getClass().getSimpleName() + "'");
	}

	public void onDriverDoorOpenChanged(boolean isDoorOpen) {

	}

	public void onPassengerDoorOpenChanged(boolean isDoorOpen) {

	}

	public void onRearLeftDoorOpenChanged(boolean isDoorOpen) {
    log.info("Rear left door opened changed to {}.", isDoorOpen);

	  try {
			this.updatePropertyValue("open", isDoorOpen);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	public void onRearRightDoorOpenChanged(boolean isDoorOpen) {

	}
}
