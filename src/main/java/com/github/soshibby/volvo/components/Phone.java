package com.github.soshibby.volvo.components;

import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.media.MediaECU;
import com.github.soshibby.volvo.canbus.handler.listeners.PhoneListener;

@org.conductor.component.annotations.Component(type = "phone")
public class Phone extends Component implements PhoneListener {
	private static Logger log = LogManager.getLogger(Phone.class.getName());

	public Phone(Database database, Map<String, Object> options, MediaECU ecu) {
		super(database, options);
		ecu.getPhone().addListener(this);
	}

	@Property(initialValue = "false")
	public void setAnswerCallPressed(Boolean answerCall) throws Exception {
		throw new Exception("Can't set property 'answerCallPressed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Property(initialValue = "false")
	public void setRejectCallPressed(Boolean answerCall) throws Exception {
		throw new Exception("Can't set property 'rejectCallPressed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Override
	public void onAnswerCallChanged(boolean answerCall) {
	  log.info("Answer call button pressed changed to {}.", answerCall);

	  try {
			this.updatePropertyValue("answerCallPressed", answerCall);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void onRejectCallChanged(boolean rejectCall) {
	  log.info("Reject call button pressed changed to {}.", rejectCall);

		try {
			this.updatePropertyValue("rejectCallPressed", rejectCall);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

}