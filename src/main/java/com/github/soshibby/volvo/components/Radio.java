package com.github.soshibby.volvo.components;

import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.media.MediaECU;
import com.github.soshibby.volvo.canbus.handler.listeners.RadioListener;

@org.conductor.component.annotations.Component(type = "phone")
public class Radio extends Component implements RadioListener {
	private static Logger log = LogManager.getLogger(Radio.class.getName());

	public Radio(Database database, Map<String, Object> options, MediaECU ecu) {
		super(database, options);
		ecu.getRadio().addListener(this);
	}

	@Property(initialValue = "false")
	public void setNextTrackPressed(Boolean nextTrack) throws Exception {
		throw new Exception("Can't set property 'nextTrackPressed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Property(initialValue = "false")
	public void setPreviousTrackPressed(Boolean previousTrack) throws Exception {
		throw new Exception("Can't set property 'previousTrackPressed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Property(initialValue = "false")
	public void setIncreaseVolumePressed(Boolean increaseVolume) throws Exception {
		throw new Exception("Can't set property 'increaseVolumePressed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Property(initialValue = "false")
	public void setDecreaseVolumePressed(Boolean decreaseVolume) throws Exception {
		throw new Exception("Can't set property 'decreaseVolumePressed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Override
	public void onNextTrackPressed(boolean nextTrackPressed) {
	  log.info("Next track button pressed is set to {}.", nextTrackPressed);

	  try {
			this.updatePropertyValue("nextTrackPressed", nextTrackPressed);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void onPreviousTrackPressed(boolean previousTrackPressed) {
    log.info("Previous track button pressed is set to {}.", previousTrackPressed);

	  try {
			this.updatePropertyValue("previousTrackPressed", previousTrackPressed);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void onIncreaseVolumePressed(boolean increaseVolumePressed) {
    log.info("Increase volume button pressed is set to {}.", increaseVolumePressed);

    try {
			this.updatePropertyValue("increaseVolumePressed", increaseVolumePressed);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void onDecreaseVolumePressed(boolean decreaseVolumePressed) {
    log.info("Decrease volume button pressed is set to {}.", decreaseVolumePressed);

	  try {
			this.updatePropertyValue("decreaseVolumePressed", decreaseVolumePressed);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

}