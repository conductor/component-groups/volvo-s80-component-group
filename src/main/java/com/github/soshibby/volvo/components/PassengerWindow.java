package com.github.soshibby.volvo.components;

import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.passengerdoor.PassengerDoorECU;
import com.github.soshibby.volvo.canbus.handler.listeners.WindowListener;
import com.github.soshibby.volvo.canbus.types.enums.WindowPosition;

@org.conductor.component.annotations.Component(type = "window")
public class PassengerWindow extends Component {
	private static Logger log = LogManager.getLogger(PassengerWindow.class.getName());
	private PassengerDoorECU passengerDoorECU;

	public PassengerWindow(Database database, Map<String, Object> options, PassengerDoorECU passengerDoorECU) {
		super(database, options);
		this.passengerDoorECU = passengerDoorECU;
		this.passengerDoorECU.getWindow().addListener(windowListener);
	}

	@Property(enums = { "up", "almostUp", "almostDown", "down" }, initialValue = "up")
	public void setPosition(String position) throws Exception {
	  log.info("Moving passenger window to position {}.", position);

	  switch (position) {
		case "up":
		case "almostUp":
			this.passengerDoorECU.getWindow().moveUp();
			break;
		case "down":
		case "almostDown":
			this.passengerDoorECU.getWindow().moveDown();
			break;
		}
	}

	private WindowListener windowListener = new WindowListener() {

		public void onWindowPositionChanged(WindowPosition windowPosition) {

		  try {
				String position = "";

				switch(windowPosition) {
				case UP:
					position = "up";
					break;
				case ALMOST_UP:
					position = "almostUp";
					break;
				case ALMOST_DOWN:
					position = "almostDown";
					break;
				case DOWN:
					position = "down";
					break;
				}

				log.info("Passenger window position changed to {}.", position);

				updatePropertyValue("position", position);
			} catch (Exception e) {
				log.error("Failed to update property value.", e);
				reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
			}
		}

	};

}

