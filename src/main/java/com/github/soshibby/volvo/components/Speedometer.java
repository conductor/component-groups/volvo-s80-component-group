package com.github.soshibby.volvo.components;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.central.CentralECU;
import com.github.soshibby.volvo.canbus.handler.listeners.SpeedometerListener;
import com.github.soshibby.volvo.canbus.utils.DateUtil;

@org.conductor.component.annotations.Component(type = "speedometer")
public class Speedometer extends Component implements SpeedometerListener {
	private static Logger log = LogManager.getLogger(Speedometer.class.getName());
	private Date lastSpeedometerChanged = new Date();

	public Speedometer(Database database, Map<String, Object> options, CentralECU ecu) {
		super(database, options);
		ecu.getSpeedometer().addListener(this);
	}

	@Property(initialValue = "0")
	public void setCurrentSpeed(Integer currentSpeed) throws Exception {
		throw new Exception("Can't set property 'currentSpeed' of component '" + this.getClass().getSimpleName() + "'");
	}

	@Override
	public void onCurrentSpeedChanged(int currentSpeed) {
	  if(lastSpeedometerChanged.after(DateUtil.addSeconds(new Date(), -10))) {
	    return;
	  } else {
	    lastSpeedometerChanged = new Date();
	  }

	  log.info("Speedometer value changed to {}.", currentSpeed);

	  try {
			this.updatePropertyValue("currentSpeed", currentSpeed);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}
}
