package com.github.soshibby.volvo.components;

import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.component.annotations.Property;
import org.conductor.component.types.Component;
import org.conductor.database.Database;

import com.github.soshibby.volvo.canbus.handler.ecu.central.CentralECU;
import com.github.soshibby.volvo.canbus.handler.listeners.AlarmListener;

@org.conductor.component.annotations.Component(type = "alarm")
public class Alarm extends Component implements AlarmListener {
	private static Logger log = LogManager.getLogger(Alarm.class.getName());

	public Alarm(Database database, Map<String, Object> options, CentralECU ecu) {
		super(database, options);
		log.info("Init alarm");
		ecu.getAlarm().addListener(this);
	}

	@Property(initialValue = "false")
	public void setTriggered(Boolean isAlarmTriggered) throws Exception {
		throw new Exception("Can't set property 'triggered' of component '" + this.getClass().getSimpleName() + "'");
	}

	public void onAlarmTriggeredChanged(boolean isAlarmTriggered) {
	  log.info("Alarm triggered changed to {}.", isAlarmTriggered);

		try {
			this.updatePropertyValue("triggered", isAlarmTriggered);
		} catch (Exception e) {
			log.error("Failed to update property value.");
			this.reportError("Failed to update property value. " + e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}
}
